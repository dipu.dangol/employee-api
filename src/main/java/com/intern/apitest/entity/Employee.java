package com.intern.apitest.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Employee {
    @Id
    @GeneratedValue
    private Long id;

    private String fullName;

    private String address;

    private String email;

    private String phoneNumber;
}
